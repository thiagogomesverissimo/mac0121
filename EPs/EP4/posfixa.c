/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Gomes Verissimo
  NUSP: 5385361

  posfixa.c
  Pitao II

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:

  - função mallocc retirada de: 

  http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/*
  NAO EDITE OU MODIFIQUE NADA QUE ESTA ESCRITO NESTE ESQUELETO
*/

/*------------------------------------------------------------*/
/* iterface para o uso da funcao deste módulo */
#include "posfixa.h"

/*------------------------------------------------------------*/
#include "categorias.h" /* Categoria, MAX_OPERADORES, INDEFINIDA, 
                           ABRE_PARENTESES, ... */
#include <stdlib.h>
#include "objetos.h" /* tipo CelObjeto, freeObjeto(), freeListaObjetos() */
#include "stack.h"   /* stackInit(), stackFree(), stackPop() 
                        stackPush(), stackTop() */



/*temporário, quero fazer um teste*/
extern char *operador[MAX_OPERADORES];

/*-------------------------------------------------------------
 *  infixaParaPosfixa
 * 
 *  Recebe uma lista ligada com cabeca INIINFIXA representando uma
 *  fila de itens de uma expressao infixa e RETORNA uma lista ligada
 *  com cabeca contendo a fila com a representacao da correspondente
 *  expressao em notacao posfixa.
 */
 /*  As celulas da notacao infixa que nao forem utilizadas na notacao 
  *  posfixa (abre e fecha parenteses) devem ser liberadas 
  *  (freeObjeto()).
  */
CelObjeto *
infixaParaPosfixa(CelObjeto *iniInfixa)
{
    /* Inicializando uma pilha para guarda os operadores da conversão */
    Stack pilha;

    /* Percorrendo a lista iniInfixa com cabeça */
    CelObjeto *p, *q, *aux; 

    /* Lista iniPosfixa será com cabeça */
    CelObjeto *iniPosfixa, *cabeca;
    /* alocando cabeça */
    cabeca = (CelObjeto*) malloc(sizeof(CelObjeto));
    cabeca->prox = NULL;
    /* iniPosfixa apontara para cabeça */
    iniPosfixa = cabeca;
  
    pilha = stackInit();
    p = iniInfixa->prox;

    while(p != NULL){
        q = p->prox;

        switch(p->categoria){
            case FLOAT: 
            case ID:
                p->prox = iniPosfixa->prox; 
                iniPosfixa->prox = p;
            break;

            case ABRE_PARENTESES:
            case ABRE_COLCHETES:
                stackPush(pilha,p);
            break;

            case OPER_ATRIBUICAO:
                stackPush(pilha,p);
            break;

            case FECHA_PARENTESES:
                while(!stackEmpty(pilha) && stackTop(pilha)->categoria != ABRE_PARENTESES){
                    /* coloco p na lista iniPosfixa*/
                    aux = stackPop(pilha);
                    aux->prox = iniPosfixa->prox; 
                    iniPosfixa->prox = aux;
                }
                free(p);
            break;

            case FECHA_COLCHETES:
                while(!stackEmpty(pilha) && stackTop(pilha)->categoria != ABRE_COLCHETES){
                    /* coloco p na lista iniPosfixa*/
                    aux = stackPop(pilha);
                    aux->prox = iniPosfixa->prox; 
                    iniPosfixa->prox = aux;
                }
                free(p);
            break;

            case OPER_ADICAO:
            case OPER_SUBTRACAO:
                while(!stackEmpty(pilha) && stackTop(pilha)->categoria != ABRE_COLCHETES){
                    /* coloco p na lista iniPosfixa*/
                    aux = stackPop(pilha);
                    aux->prox = iniPosfixa->prox; 
                    iniPosfixa->prox = aux;                    
                }
                stackPush(pilha,p);
            break;

            case OPER_DIVISAO:
            case OPER_MULTIPLICACAO:
                while(!stackEmpty(pilha) && 
                        stackTop(pilha)->categoria != ABRE_COLCHETES && 
                        stackTop(pilha)->categoria != OPER_SUBTRACAO &&
                        stackTop(pilha)->categoria != OPER_ADICAO){
                    /* coloco p na lista iniPosfixa*/
                    aux = stackPop(pilha);
                    aux->prox = iniPosfixa->prox; 
                    iniPosfixa->prox = aux;                    
                }
                stackPush(pilha,p);
            break;

            default:
                /*printf("entrei aqui\n");*/
            break;
        }
        p = q;
    }

    /* operadores restantes */
    while(!stackEmpty(pilha)){
        aux = stackPop(pilha);
        aux->prox = iniPosfixa->prox; 
        iniPosfixa->prox = aux;
    }

    inverter(&iniPosfixa->prox);

    return iniPosfixa;
}

void inverter(CelObjeto **ini){
    CelObjeto *p, *q, *r;
    p = NULL;
    q = *ini;

    while(q != NULL){
        r = q->prox;
        q->prox = p;
        p = q;
        q = r;
    }
    *ini = p;
}

/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Gomes Verissimo
  NUSP: 5385361

  stack.c
  Pitao II

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.

  Exemplo:
  - função mallocc retirada de: 
 
  http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/* 
 * 
 * STACK.c: IMPLEMENTACAO DE UMA PILHA 
 *
 * TAREFA EP3
 *
 * Faca aqui a implementacao de uma pilha atraves de uma 
 * __lista encadeada com cabeca__. 
 * 
 * A implementacao deve respeitar a interface que voce declarar em 
 * stack.h. 
 *
 * TAREFA EP4: copiar a implementacao feira para o EP3.
 *     E possivel que sua implementacao para o EP3 deve ser 
 *     adaptada, ja que agora a pilha de execucao pode conter
 *     nomes de variaveis e nao apenas valores double.
 *
 */

/* interface para o uso de uma pilha */
#include<stdlib.h>
#include "objetos.h"
#include "stack.h"
#include "categorias.h"

/* 
 * 
 * STACK.c: IMPLEMENTACAO DE UMA PILHA 
 *
 * TAREFA
 *
 * Faca aqui a implementacao de uma pilha atraves de uma 
 * __lista encadeada com cabeca__. 
 * 
 * A implementacao deve respeitar a interface que voce declarar em 
 * stack.h. 
 *
 */ 

typedef struct stacknode* Link;

struct stacknode { 
  Item conteudo; 
  Link prox; 
};

struct stack {
  Link topo;
};


Stack stackInit()
{
  Stack s = (Stack) malloc(sizeof *s);
  s->topo = NULL; 
  return s;
}

int
stackEmpty(Stack s)
{
  return s->topo == NULL; 
}

void
stackPush(Stack s, Item conteudo)
{
  Link p = (Link) malloc(sizeof *p);
  p->conteudo = conteudo;
  p->prox = s->topo;
  s->topo = p;
}

Item 
stackPop(Stack s)
{ 
  Link p = s->topo;
  Item conteudo;

  if (p == NULL) /* stackempty() */
    {
      fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
      exit(-1);
    }
  /* tudo bem, a pilha nao esta vazia... */
  conteudo = p->conteudo;
  s->topo = p->prox;
  free(p);
  return conteudo;  
}

Item
stackTop(Stack s)
{
  if (s->topo == NULL) /* stackempty() */
    {
      fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
      exit(-1);
    }

  /* tudo bem, a pilha nao esta vazia... */
  return  s->topo->conteudo;
}

void 
stackFree(Stack s)
{
  while (s->topo != NULL) 
    {
      Link p = s->topo;
      s->topo = s->topo->prox;
      free(p);
    }

  free(s);
}

void
stackDump(Stack s)
{
  Link p = s->topo;
  if (p == NULL) fprintf(stdout, "vazia.");
  printf("\nPilha de execução: ");

  while (p != NULL)
    {
      switch(p->conteudo->categoria){
      
        case FLOAT:
            printf("%.2f ", p->conteudo->valor.vFloat);
        break;
        case ID:
            printf("'%s' ", p->conteudo->nomeID);
        break;
        default:
        break;        
      }
      p = p->prox;
    }

  printf("\n");
}



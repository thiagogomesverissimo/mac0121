/*
 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Gomes Veríssimo
  NUSP: 5385361

  stack.c
  Pitao I

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma referência, liste-as abaixo
  para que o seu programa não seja considerado plágio.

  Exemplo:
  - função mallocc retirada de: 
 
    http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/* interface para o uso de uma pilha */
#include<stdlib.h>
#include "objetos.h"
#include "stack.h"

/* 
 * 
 * STACK.c: IMPLEMENTACAO DE UMA PILHA 
 *
 * TAREFA
 *
 * Faca aqui a implementacao de uma pilha atraves de uma 
 * __lista encadeada com cabeca__. 
 * 
 * A implementacao deve respeitar a interface que voce declarar em 
 * stack.h. 
 *
 */

struct stackNode {
    Item conteudo;
    struct stackNode* prox;
};
typedef struct stackNode* Link;

static Link topo;

void stackInit(){
    topo = (Link) malloc(sizeof *topo);
    topo->prox = NULL;
}

int stackEmpty(){
    return topo->prox == NULL;
}

void stackPush(Item item) {
    Link nova = (Link) malloc(sizeof *nova);
    nova->conteudo = item;
    nova->prox = topo->prox;
    topo->prox = nova;
}

Item stackPop() {

    Link p = topo->prox;
    Item item;
    
    if (topo->prox == NULL) {
        fprintf(stderr,"Putz, voce nao sabe o que esta fazendo, pilha vazia!\n");
        exit(-1);
    }
    item = p->conteudo; 
    topo->prox = p->prox;
    free(p);
    return item;
}

Item stackTop(){
    return (topo->prox)->conteudo;
}

void stackFree(){
    Link p;
    p = topo->prox;
    while(p != NULL){
        Link eliminada;
        eliminada = p->prox;
        free(eliminada);
        p = p->prox;
    }
}


void stackDump(){

    Link p;
    if(stackEmpty()) printf("Pilha atualmente vazia.");

    p = topo->prox;
    printf("Estado atual da pilha: \n");
    while(p != NULL){
        printf("%.2f ", p->conteudo->valor.vFloat);
        p = p->prox;
    }
    fprintf(stdout, "\n");
}



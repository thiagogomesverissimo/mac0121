/*\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Thiago Gomes Veríssimo
  NUSP: 5385361

  ep1.c

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:

  - função randomInteger() de: 

  http://www.ime.usp.br/~pf/algoritmos/aulas/random.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__*/

/*
 *  Sobre os nomes da variaveis:
 *
 *  Adotamos a especificacao das variaveis em (lower) "camel case":
 * 
 *       http://en.wikipedia.org/wiki/Camel_case
 *
 *  Sobre as especificações das funções:
 *
 *  Nos protótipos das funções, os nomes dos parametros
 *  seguem o padrao camel case. No entanto, nas especificações,
 *  esses nomes aparecem vertidos para maiúsculas para destacar 
 *  o papel de cada parâmetro. Isso corresponde a recomendacao do 
 *  GNU Coding Standards:
 *
 *       http://www.gnu.org/prep/standards/standards.html#Comments
 */

#include <stdio.h>  /* scanf(), printf(), ... */
#include <stdlib.h> /* srand(), rand(), atoi(), exit(), ...  */
#include <string.h> /* strlen(), strcmp(), ... */  

/*---------------------------------------------------------------*/
/* 
 * -1. ESTRUTURAS
 */

typedef struct {
    int lin;
    int col;
} TAPA;

/*---------------------------------------------------------------*/
/* 
 * 0. C O N S T A N T E S 
 */

/* tamanho máximo de um turtledorm */
#define MAX      128

/* estado da turtle */
#define ACORDADO   '#'
#define DORMINDO   ' '
#define TAPINHA    'T'

#define TRUE       1
#define FALSE      0

#define ENTER      '\n'
#define FIM        '\0'
#define ESPACO     ' '


/*---------------------------------------------------------------*/
/*
 * 1. P R O T Ó T I P O S
 */

/* PARTE I */
void m();

void
leiaTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

void 
mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c);

void
tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col);

int 
todosDormindo(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE II */
void
sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

int
graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE III */

void
resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* FUNÇõES AUXILIARES */
int 
randomInteger(int low, int high);

TAPA
preparaTapa(char c[]);

int 
despertos(int nLin, int nCol, int tDorm[][MAX]);

int
chasing (int nLin, int nCol, int tDorm[][MAX], int tapinhas[][MAX]);

void
incremente(int bin[]);

void 
pause();


/*---------------------------------------------------------------*/
/* 
 *  M A I N 
 */

int 
main(int argc, char *argv[])
{
    char tipo_inicio;
    char escolha[5];
    int nLin,nCol;
    int tDorm[MAX][MAX];
    int tentativas = 0;
    TAPA tapa;

    printf("### Bem-vindo ao Dormedorme Turtle! ###\n");
    printf("Você acaba de se tornar a(o) guardiã(o) do turtledorm.\n");
    printf("\nDigite 's' para sortear turtledorm ou\n");
    printf("       'l' para ler um arquivo de turtledorm: ");
    scanf("%s", &tipo_inicio);

    if ( tipo_inicio == 'l') {
        leiaTurtledorm(&nLin, &nCol, tDorm);
    } else if ( tipo_inicio == 's') {
        sorteieTurtledorm(&nLin, &nCol,tDorm);
    }
    
    while(1) {
        mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);        
        if(todosDormindo(nLin, nCol, tDorm)) {
            printf("Parabéns, você colocou todos para dormir após %d tapinha(s)!\n", tentativas);
            printf("Todos já estão dormindo. Nao faça barulho!\n\n");
            exit(0);
        }

        printf("Digite: 'd' para desistir, \n");
        printf("        'a' para receber ajuda para encontrar uma solução \n");
        printf("        'g' para gravar o turtleDorm atual em um arquivo \n");
        printf("        'linha coluna' para dar um tapinha, exemplo: 3 1\n");

        /* Vou usar fgets ao invés de scanf por conta da possibilidade
         * de ter espaços ou não para a resposta dessa pergunta
         * Limpa linhas antes da leitura do fgets
         **/
        scanf(" ");
        fgets(escolha, 50, stdin);

        switch(*escolha)
        {
            case 'd':
                printf("Voce desejou sair!\n");
                printf("Números de tentativas: %d\n",tentativas);
                exit(0);
            case 'a':
                resolvaTurtledorm(nLin, nCol, tDorm);
                break;
            case 'g':
                graveTurtledorm(nLin, nCol, tDorm);
                break;
            default:
                tentativas++;
                tapa = preparaTapa(escolha);
                tapinhaTurtle(nLin, nCol, tDorm, tapa.lin, tapa.col);
        }
    }

    return EXIT_SUCCESS;
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                        P A R T E   I 
 */
void leiaTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]) {
    FILE *fp;
    char arquivo[256];

    /* indices para varrer turtleDorm */
    int i,j;

    printf("Digite um nome de arquivo (exemplo: a.txt): ");
    scanf("%s", arquivo);
    printf("\n");
    fp = fopen(arquivo,"r");

    /* Lendo dimensão */
    fscanf(fp,"%d %d\n",nLin, nCol);

    /* Lendo a matriz */
    for(i = 0; i < *nLin; i++){
        for(j = 0; j < *nCol; j++){
            fscanf(fp,"%d ",&tDorm[i][j]);
        }
    }
    fclose(fp);
}

void mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c){

    /* indices para varrer turtleDorm */
    int i,j;

    /* Vizualização "bonita" para o turtleDorm */
    printf("\n");
    printf("    ");
    for(j = 0; j < nCol; j++){
        printf("   %d  ",j+1);
    }
    printf("\n");
    for(i = 0; i < nLin; i++){
        printf("    ");
        for(j = 0; j < nCol; j++){
            printf("+-----");
        }
        printf("+\n %d  ",i+1);
        for(j = 0; j < nCol; j++){
            /* aqui mostramos se o turtle está dormindo ou acordado */
            if(tDorm[i][j] == 0) {
                printf("|  %c  ",DORMINDO);
            } else {
                printf("|  %c  ",c);
            }
        }
        printf("|\n");
    }
    printf("    ");
    for(j = 0; j < nCol; j++){
        printf("+-----");
    }
    printf("+\n\n");
}

void
tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col) {

    /* corrigindo tapa, pois o array da matriz começa em 0 */
    lin--;
    col--;
    if( (lin >= 0 && lin < nLin) && (col >= 0 && col < nCol)) {

        /* Acorda ou faz dormir turtlena posição indicada */
        if(tDorm[lin][col] == 0) tDorm[lin][col] = 1;
        else tDorm[lin][col] = 0;

        /* Acima: primeiro verificamos se existe turtle acima */
        if( (lin - 1) >= 0) {
            if(tDorm[lin-1][col] == 0) tDorm[lin-1][col] = 1;
            else tDorm[lin-1][col] = 0;
        }

        /* Abaixo: primeiro verificamos se existe turtle abaixo */
        if( (lin + 1) < nLin) {
            if(tDorm[lin+1][col] == 0) tDorm[lin+1][col] = 1;
            else tDorm[lin+1][col] = 0;
        }

        /* Esquerda: primeiro verificamos se existe turtle a esquerda */
        if( (col - 1) >= 0) {
            if(tDorm[lin][col-1] == 0) tDorm[lin][col-1] = 1;
            else tDorm[lin][col-1] = 0;
        }

        /* Direita: primeiro verificamos se existe turtle a direita */
        if( (col + 1) < nCol) {
            if(tDorm[lin][col+1] == 0) tDorm[lin][col+1] = 1;
            else tDorm[lin][col+1] = 0;
        }

    } else {
        printf(" ################################################## \n\n");
        printf(" #### Posição do tapinha inválida, tente outra! ### \n\n");
        printf(" #### linha: %d, coluna: %d ### \n\n", lin,col);
        printf(" ################################################## \n\n");
    }
}

int 
todosDormindo(int nLin, int nCol, int tDorm[][MAX]) {

    /* indices para varrer turtleDorm */
    int i,j;

    int acabou = TRUE;
    for(i = 0; i < nLin ; i++){
        for(j = 0; j < nCol ; j++){
            if(tDorm[i][j] == 1) acabou = FALSE;
        }
    }
    return acabou;
}

int
graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]){

    FILE *fp;
    char arquivo[256];

    /* indices para varrer turtleDorm */
    int i,j;

    printf("Digite o nome do arquivo para salvar o turtledorm: ");
    scanf("%s",arquivo);

    fp = fopen(arquivo,"w+");
    if(!fp) return EXIT_FAILURE;

    /* Na primeira linha, consta a dimensão */
    fprintf(fp,"%d %d\n",nLin,nCol);

    for(i = 0; i < nLin; i++){
        for(j = 0; j < (nCol -1); j++){
            fprintf(fp,"%d ",tDorm[i][j]);
        }
        /** O último elemento de cada linha está fora do loop 
          * acima para não sair com espaço no final */
        fprintf(fp,"%d\n",tDorm[i][nCol-1]);
    }
    fclose(fp);

    return EXIT_SUCCESS;
}

/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                       P A R T E   II 
 */
void
sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]) {

    /* indices para varrer turtleDorm */
    int i,j;

    /* Tamanho do TurtleDorm */
    int n;

    char dificuldade;
    int semente;
    int nTapinhas;

    /* Intervalo para sorteio do número de tapinhas */
    int range_inicio = 0;
    int range_fim = 0;

    /* Dimensão sorteada */
    TAPA sorteado;

    printf("Qual a dimensão do turtledorm. (Exemplo: 5 5): ");
    scanf("%d %d", nLin, nCol);

    /* Tamanho do TurtleDorm linhas x colunas */
    n = *nLin * *nCol;

    printf("Digite um inteiro para o gerador de números aleatórios (semente): ");
    scanf("%d", &semente);
    srand(semente);

    printf("Qual o nível de dificuldade [f/m/d]: ");
    scanf("%s", &dificuldade);

    /* Inicializa turtleDorm com zeros */
    for(i = 0; i < *nLin; i++){
        for(j = 0; j < *nCol; j++){
            tDorm[i][j] = 0;
        }
    }
    
    /* sortear posições para dar tapinhas */
    switch(dificuldade)
    {
        case 'f':
            range_inicio = (int) (0.05 * n);
            range_fim = (int) (0.2 * n);
        break;

        case 'm':
            range_inicio = (int) (0.25 * n);
            range_fim = (int) (0.5 * n);
        break;

        case 'd':
            range_inicio = (int) (0.55 * n);
            range_fim = (int) (0.85 * n);
        break;
    }
    nTapinhas = randomInteger(range_inicio, range_fim);

    /* Dar tapinhas nos turltes de inicialização */
    for(i = 0; i < nTapinhas; i++) {
        sorteado.lin = randomInteger(1, *nLin);
        sorteado.col = randomInteger(1, *nCol);
        tapinhaTurtle(*nLin, *nCol, tDorm, sorteado.lin, sorteado.col);
    }

    printf("Número de tapinhas sorteados: %d\n",nTapinhas);
    printf("Número de turtles despertos no momento: %d\n",despertos(*nLin,*nCol,tDorm));
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                       P A R T E   III 
 */

void
resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]){

    /* indíces para varrermos o turtleDorm */   
    int i,j;

    /** numero binario será número de nCol + 1 bits. 
     * o bit adicional é necessário para sair do "while"
     * pois só vamos varrer a primeira linha
     **/
    int bin[MAX+1];

    /** numero de bits 
     *  durante o programa nBits == nCol 
     **/
    int nBits;

    int tapinhas[MAX][MAX]; /* representacao da lista de tapinhas */

    /* matriz de representacao da lista de tapinhas para guardar 
       solução mais eficiente até então */
    int solucao[MAX][MAX];

    /* total de tapinhas dados na última solução encontrada, zero se não teve solução */
    int total_de_tapinhas_ultima_solucao = 0;

    /* total de soluções encontrada */
    int total_de_solucoes = 0;

    int retorno_do_chasing;

    /* inicialize bin[] com zeros */
    nBits = nCol;
    for (i = 0; i < nBits+1; i++)
    {
        bin[i] = 0; 
    } 

    /** A saída desse while será justamente 
      * quando chegarmos o bit adicional
     **/
    while (bin[nBits] == 0) 
    {     
        /* zera matriz de tapinhas */
        for (i = 0; i < nLin; i++) {
            for (j = 0; j < nCol; j++) {
                tapinhas[i][j] = 0;
            }
        }

        /* transforme os nLin*nCol (== nBits-1) bits menos significativos de 
         * bin em uma matriz binaria de dimensao nLin x nCol
         */
        i = 0;
        for (j = 0; j < nCol; j++) {
            tapinhas[i][j] = bin[i*nCol + j];
        }

        /* 2. Chamar função chasing tDormAuxiliar que retorna inteiro diferente de zero
           caso encontre uma solução */
        retorno_do_chasing = chasing(nLin, nCol, tDorm, tapinhas);
        if (retorno_do_chasing != 0) {
            total_de_solucoes++;
            /* Se total_de_tapinhas_ultima_solucao for igual a zero, essa é a primeira solução encontrada,
              ou se o total de tapinhas retornados for menor que a última solução, guardamos a solução*/
            if(total_de_tapinhas_ultima_solucao == 0 || retorno_do_chasing < total_de_tapinhas_ultima_solucao) {
                total_de_tapinhas_ultima_solucao = retorno_do_chasing;
                /* copia tapinhas da solução para matriz solução */
                for(i = 0; i < nLin; i++){
                    for(j = 0; j < nCol; j++){
                        solucao[i][j] = tapinhas[i][j];
                    }
                }
            }
        }
        /* incremente o numero binario em bin */
        incremente(bin);
    }

    /*Informando o usuário se alguma solução foi encontrada*/
    if(total_de_solucoes == 0) {
        printf("##### Nao tem solucao!  ######\n");
    } else {
        printf("##### %d solução(ões) encontrada(s).  ######\n",total_de_solucoes);
        printf("##### A melhor solução encontrou %d tapinhas: \n", total_de_tapinhas_ultima_solucao);
        mostreTurtledorm(nLin, nCol, solucao, TAPINHA);
        printf("##### Retornando a TurtleDorm ######\n");
    }
}
 
/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                     A U X I L I A R E S 
 */

/**
 * Função auxiliar que receber a string com linha e coluna do tapa e
 * retorna a estrutura TAPA
 */
TAPA preparaTapa(char *str){
    TAPA tapa;

    char *lin;
    char *col;

    lin =  strtok(str," ");
    col =  strtok(NULL," ");

    tapa.lin = atoi(lin);
    tapa.col = atoi(col);

    return tapa;
}

/** Função auxiliar que computa número de turtles despertos 
 * em um turtleDorm 
 **/
int despertos(int nLin, int nCol, int tDorm[][MAX]) {

    /* indices para varrer turtleDorm */
    int i,j;

    int despertos = 0;
    for(i = 0; i < nLin; i++){
        for(j = 0; j < nCol; j++){
            despertos += tDorm[i][j];
        }
    }
    return despertos;
}

/** Função auxiliar que soliciona usando método chasing 
 *  descrito em: https://en.wikipedia.org/wiki/Lights_Out_(game)
 *  A variável tapinhas irá computar os tapinhos dados por força 
 *  bruta mais os dados por chasing. 
 **/
int
chasing(int nLin, int nCol, int tDorm[][MAX], int tapinhas[][MAX]) {

    /* indices para varrer turtleDorm */
    int i,j;

    /* total de tapinhas dados em um dada solução, zero se não teve solução*/
    int total_de_tapinhas = 0;

    /* matriz auxiliar para não alterarmos a original*/
    int tDormAuxiliar[MAX][MAX];

    /* copia tDorm para tDormAuxiliar */
    for(i = 0; i < nLin; i++){
        for(j = 0; j < nCol; j++){
            tDormAuxiliar[i][j] = tDorm[i][j];
        }
    }

    /* Dar tapinhas em tDormAuxiliar nas posições de tapinhas[0][j] */
    for(j = 0; j < nCol; j++){
        if(tapinhas[0][j] == 1)
            tapinhaTurtle(nLin, nCol, tDormAuxiliar, 1, j+1);
    }

    /* varrer da segunda linha em diante para fazer o chasing*/
    for(i = 1; i < nLin; i++){
        for(j = 0; j < nCol; j++){
            /* Verificamos se na linha de cima da posição atual
             * tem um turtle acordado e que deve dormir*/

            /* Se o turtle de cima está acordado, vamos dar um tapinha no de baixo 
               para fazer o de cima dormir */
            if(tDormAuxiliar[i-1][j] == 1){
                /* Vamos marcar a posição que o tapinha será dado */
                tapinhas[i][j] = 1;
                /* Dar de fato o tapinha */
                tapinhaTurtle(nLin, nCol, tDormAuxiliar, i+1, j+1);
                
                /* Verificando se neste momento todos turtles estão dormindo e
                   encontramos assim uma solução. */
                if(despertos(nLin, nCol, tDormAuxiliar) == 0) {
                    /* Com a função auxuliar despertos()
                       podemos verificar quantos tapinhas foram dados na solução encontrada. */
                    total_de_tapinhas = despertos(nLin, nCol, tapinhas);
                    return total_de_tapinhas;
                }
            }
        }
    }
    return total_de_tapinhas;
}

/* 
 * randomInteger()
 *
 * A função recebe dois inteiros LOW e HIGH e retorna um 
 * inteiro aleatório entre LOW e HIGH inclusive, ou seja, 
 * no intervalo fechado LOW..HIGH.
 *
 * Pré-condição: a função supõe que 0 <= LOW <= HIGH < INT_MAX.
 *     O codigo foi copiado da página de projeto de algoritmos 
 *     de Paulo Feofiloff, que por sua vez diz ter copiado o 
 *     código da biblioteca random de Eric Roberts.
 */
int 
randomInteger(int low, int high)
{
    int k;
    double d;
    d = (double) rand( ) / ((double) RAND_MAX + 1);
    k = d * (high - low + 1);
    return low + k;
}

/* 
 * pause()
 *
 * Para a execucao do programa ate que um ENTER seja digitado.
 */
void 
pause()
{
    char ch;

    printf("Digite ENTER para continuar. ");
    do 
    {
        scanf("%c", &ch);
    }
    while (ch != ENTER);
}

/* 
 * incremente(bin)
 * 
 * Recebe atraves do vetor BIN a representacao de um 
 * número binario k e devolve em BIN a representacao 
 * binaria de k+1.
 * 
 * Pre-condicao: a funcao nao se preocupa com overflow,
 *   ou seja, supoe que k+1 pode ser representado em 
 *   BIN.
 */ 
void
incremente(int bin[])
{
    int i;

    for (i = 0; bin[i] != 0; i++)
        bin[i] = 0;

    bin[i] = 1;
}


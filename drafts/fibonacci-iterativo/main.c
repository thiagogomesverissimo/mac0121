# include <stdio.h>
# include <stdlib.h>

long fibonacci(int n);

void main(int argc, char *argv[]) {
    int n;
    if (argc != 2) { 
        exit(0);
    }
    n = atoi(argv[1]);
    printf("fibonacci of %d is %d\n",n,fibonacci(n));
}

long fibonacci(int n) {
    long ultimo, penultimo, auxiliar;

    if (n==0 || n==1) return n;

    penultimo = 0;
    ultimo = 1;

    for (int i = 2; i <= n ; i++) {
        auxiliar = ultimo + penultimo;
        penultimo = ultimo;
        ultimo = auxiliar;
    }
    return ultimo;
}


# include <stdio.h>
# include <stdlib.h>

int fibonacci(int n);

void main(int argc, char *argv[]){
    int n;
    if(argc != 2) {
        printf("run example: ./a.out 6\n");
        exit(0);
    }
    n = atoi(argv[1]);
    printf("Fibonacci of %d is %d\n",n ,fibonacci(n));
}

int fibonacci(int n) {
    if(n == 0 || n == 1) return n;
    return fibonacci(n-1) + fibonacci(n-2);
}

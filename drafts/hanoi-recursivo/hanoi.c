# include <stdio.h>
# include <stdlib.h>

void hanoi (int n, char origem, char auxiliar, char destino);

void main(int argc, char *argv[]){
    if(argc != 2){
        exit(0);
    }
    hanoi(atoi(argv[1]),'A','B','C');
}

void hanoi (int n, char origem, char auxiliar, char destino) {
    if(n > 0) {
        hanoi(n-1, origem, destino, auxiliar);
            printf("Mover disco %d de %c para %c\n",n,origem,destino);
        hanoi(n-1, auxiliar, origem, destino);
    }
}

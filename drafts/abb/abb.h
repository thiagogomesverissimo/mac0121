/* O conteúdo em si de cada node, nem sempre vamos usar nos exemplos */
typedef int Conteudo;

/* A chave será usada para ordenação ABB */
typedef int Chave;

typedef struct node {
    Chave chave;
    Conteudo conteudo;
    struct node *esq;
    struct node *dir;
} Node;

/* Para ficar mais fácil, vamos chamar o endereço de um nó de árvore*/
typedef Node *Arvore;

/* Métodos que serão implementados */
void dump(Arvore arvore);
void mostra(Arvore arvore, int altura);
Arvore insere(Node *node, Arvore arvore);
int altura(Arvore arvore);

/* percurso em ordem */
void erd(Arvore arvore);

/* percuro pós-ordem*/
void edr(Arvore arvore);

/*pré-ordem*/
void red(Arvore arvore);

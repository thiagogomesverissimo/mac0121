#include <stdio.h>
#include <stdlib.h>
#include "abb.h"

int main(int argc, char **argv){
   
    /* Árvore de Busca Binária - ABB. Usando a chave para comparação,
     * dizemos que um árvode é ABB se:
     *  - Todos os nós da sub-árvore direita são maiores que a raiz
     *  - Todos os nós da sub-árvore esquerda são menores que a raiz.
     *  Exemplo:
     *                      8
     *              3               10
     *          1       6                14
     *                4   7           13
     **/
    int i;
    //int chaves[] = {8,3,10,1,6,14,4,7,13}; int n = 9;
    //int chaves[] = {-1,-4,-7,-2,0,2,4,3,5,7,6,8,666,333,1332}; int n = 15;
    int chaves[] = {12,-1,31,7,3,-6,25,10,11,42}; int n = 10;
    Node *folha;

    /* Criando a raiz */
    Node *raiz = malloc(sizeof *raiz);
    raiz->chave = chaves[0];
    raiz->esq = NULL;
    raiz->dir = NULL;

    /* Inserindo novas folha*/
    for(i = 1; i < n; i++) {
        folha = malloc(sizeof *folha);
        folha->chave = chaves[i];
        folha->esq = NULL;
        folha->dir = NULL;
        raiz = insere(folha,raiz);
    }

    dump(raiz);

    printf("\nPercorrendo em InOrder esquerda-raiz-direira:\n");
    erd(raiz);
    printf("\nPercorrendo em PosOrder esquerda-direira-raiz:\n");
    edr(raiz);
    printf("\nPercorrendo em PreOrder raiz-esquerda-direita:\n");
    red(raiz);

    printf("\nAltura da árvore: %d\n", altura(raiz));

    return 0;
}

void dump(Arvore arvore) {
    int i;

    if(arvore != NULL) {

        printf("%d\n",arvore->chave);
        if(arvore->esq != NULL){
            printf("  esquerda de %d: ",arvore->chave);
                dump(arvore->esq);
        }
        if(arvore->dir != NULL ){
            printf("    direita de %d: ", arvore->chave);
            dump(arvore->dir);
        }
    }
}

/* Não funcionando */
void mostra(Arvore arvore, int altura) {
    int i;

    if(arvore != NULL) {
        //for(i = 0; i < altura; i++) printf(" ");
        //printf("%d\n",arvore->chave);

        if(arvore->esq != NULL && arvore->dir != NULL) {
            for(i = 0; i < altura; i++) printf("  ");
            //printf("%d ",arvore->esq->chave);

            for(i = 0; i < altura; i++) printf("  ");
            //printf("%d\n",arvore->dir->chave);
            mostra(arvore->esq, altura);
            mostra(arvore->dir, altura);
        }
        else if(arvore->esq != NULL && arvore->dir == NULL) {
            for(i = 0; i < altura; i++) printf("  ");
            //printf("%d ",arvore->esq->chave);
            mostra(arvore->esq, altura);
        }
        else if(arvore->esq == NULL && arvore->dir != NULL) {
            for(i = 0; i < altura; i++) printf("  ");
            //printf("%d\n",arvore->dir->chave);
            mostra(arvore->esq, altura);
        } else { 
            printf("%d ",arvore->chave);
        }
    }
}

Arvore insere(Node *node, Arvore arvore) {

    /* Verifica se chegamos numa folha */
    if(arvore == NULL)
        return node;

    /* Verifica se é filho da esquerda ou direita */
    if(node->chave < arvore->chave)
        arvore->esq = insere(node,arvore->esq);
    else 
        arvore->dir = insere(node,arvore->dir);
}

/* percurso em ordem */
void erd(Arvore arvore){
    if(arvore != NULL){
        erd(arvore->esq);
        printf("%d ",arvore->chave);
        erd(arvore->dir);
    }
}

/* percurso pós-ordem */
void edr(Arvore arvore){
    if( arvore != NULL ) {
        /* Verifico se é folha */
        if(arvore->esq == NULL && arvore->dir == NULL)
            printf("%d ",arvore->chave);
        else {
            edr(arvore->esq);
            edr(arvore->dir);
            printf("%d ",arvore->chave);
        }
    }
}

/* percurso pré-ordem*/
void red(Arvore arvore){
    if(arvore != NULL){
        /* Verifico se é folha */
        if(arvore->esq == NULL && arvore->dir == NULL)
            printf("%d ",arvore->chave);
        else {
            printf("%d ",arvore->chave);
            red(arvore->esq);
            red(arvore->dir);
        }
    }
}

/* Caminho mais longo até a última folha */

int altura(Arvore arvore){
    /* se for folha, altura 0 */
    if(arvore == NULL){
        return -1;
    }
    int altura_esquerda = altura(arvore->esq);
    int altura_direita = altura(arvore->dir);
    if(altura_esquerda > altura_direita) return altura_esquerda + 1;
    return altura_direita+1;
}

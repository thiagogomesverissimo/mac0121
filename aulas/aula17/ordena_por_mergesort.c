#include<stdio.h>
#include<stdlib.h>

void mergeSort(int p, int r, int v[]);
void intercale(int p, int q, int r, int v[]);

int main(int argc, char **argv){
    int v[] = {81,23,23,75,446,0,22,1,1,0,87,7};
    int i, n = 12;

    printf("\nVetor não ordenado: \n");
    for(i = 0; i < n;i++){
        printf("%d ",v[i]);
    }

    mergeSort(0,n,v);
    printf("\nVetor ordenado: \n");
    for(i = 0; i < n;i++){
        printf("%d ",v[i]);
    }
}

void mergeSort(int p, int r, int v[]){
    /* Tem pelo menos dois elementos */
    if( p < r-1 ){
        int q = (p+r)/2;
        /*Trecho da esquerda*/
        mergeSort(p,q,v);
        /*Trecho da direita*/
        mergeSort(q,r,v);
        intercale(p,q,r,v);
    }
}

void intercale(int p, int q, int r, int v[]){
    int i, j, k, *w;
    w = malloc((r-p)*sizeof(int));
    for(i=p,k=0; i<q; i++, k++){
        w[k] = v[i];
    }
    for(j=r-1;j>=q; j--, k++){
        w[k] = v[j];
    }
    i = 0;
    j = r-p-1;
    for(k=p;k<r; k++){
        if(w[i]<=w[j]) v[k] = w[i++];
        else v[k] = w[j--];
    }
}

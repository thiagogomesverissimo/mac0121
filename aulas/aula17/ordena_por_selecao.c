#include<stdio.h>
#include<stdlib.h>

void selecao(int,int*);

int main(int argc, char **argv){
    int v[] = {81,23,23,75,446,0,22,1,1,0,87,7};
    int i, n = 12;

    printf("\nVetor não ordenado: \n");
    for(i = 0; i < n;i++){
        printf("%d ",v[i]);
    }

    selecao(n,v);
    printf("\nVetor ordenado: \n");
    for(i = 0; i < n;i++){
        printf("%d ",v[i]);
    }
}

void selecao(int n, int v[]){
    int i, j, max, temp;
    /* Começamos no último, e não precisamos chegar na posição 0 */
    for(i = n-1; i>0;i--){
        max = i;
        /* Comparamos o item atual com todos antes dele
         * pois os posteriores já são maiores */
        for(j = i-1; j>=0;j--){
            if(v[j] > v[max]){
                max = j;
            }
        }
        /* Neste momentos encontramos o max antes de i,
         * que pode ser ele mesmo, então trocamos a posição
         * i pela max */
        temp = v[i];
        v[i] = v[max];
        v[max] = temp;
    }
}

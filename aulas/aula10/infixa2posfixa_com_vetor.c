#include<stdio.h>
#include<stdlib.h>
#include <string.h>

char *infixa2posfixa(char *infixa);

int main(int argc, char **argv) {
    char infixa[500] = "A+B*C";
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "(A+(B*C))");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A*(B+C)/D-E");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A+B*(C-D*(E-F)-G*H)-I*3");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A+B*C/D*E-F");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A+(B-(C+(D-(E+F))))");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A*(B+(C*(D+(E*(F+G)))))");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    return 0;
}

char *infixa2posfixa(char *infixa){
    char *posfixa;
    char item;
    int n;
    n = strlen(infixa);

    int pilha[n];
    int proximo_livre = 0;

    posfixa = (char*) malloc( (n+1) * sizeof(char));
    int i, j=0;

    for(i = 0; i < n; i++) {
        switch(infixa[i])
        {
            case '(':
                pilha[proximo_livre++] = infixa[i];
            break;
            case ')':
                item = pilha[--proximo_livre];
                while(item != '(') {
                    posfixa[j++] = item;
                    item = pilha[--proximo_livre];
                }
            break;
            case '+':
            case '-':
                item = pilha[proximo_livre-1];
                while(item == '+' || item == '-' || item == '/' || item == '*'){
                    posfixa[j++] = pilha[--proximo_livre];
                    if(proximo_livre == 0)
                        break;
                    item = pilha[proximo_livre-1];
                }
                pilha[proximo_livre++] = infixa[i];
            break;
            case '/':
            case '*':
                item = pilha[proximo_livre-1];
                while(item == '/' || item == '*'){
                    posfixa[j++] = pilha[--proximo_livre];
                    if(proximo_livre == 0)
                        break;
                    item = pilha[proximo_livre-1];
                }
                pilha[proximo_livre++] = infixa[i];
            break;
            default:
                posfixa[j++] = infixa[i];
        }
    }

    while(proximo_livre != 0){
        posfixa[j++] = pilha[--proximo_livre];
    }
    posfixa[n] = '\0';
    return posfixa;
}

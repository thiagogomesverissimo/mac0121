#include<stdio.h>
#include<stdlib.h>

struct celula {
    int conteudo;
    struct celula *prox;
};
typedef struct celula Celula;

/* protótipos das funções */
void inserir(int conteudo, Celula **ini);
void mostra(Celula *ini);
void remover(Celula *proxima_de_celula);
Celula *busca(int conteudo, Celula *ini);
void buscaInsere(int conteudo, int antes_conteudo, Celula **ini);
void buscaRemove(int conteudo, Celula **ini);
void inverter(Celula **ini);

int main(){
    Celula *buscado;

    /* Inicializando lista */
    Celula *ini = NULL;

    /* Função mostra funciona mesmo com lista vazia */
    mostra(ini);

    /* Inserindo valores no começo da lista */
    inserir(111, &ini);
    printf("Conteudo: %d\n",ini->conteudo);
    inserir(222, &ini);
    printf("Conteudo: %d\n",ini->conteudo);
    inserir(333, &ini);
    printf("Conteudo: %d\n",ini->conteudo);
    inserir(444, &ini);
    printf("Conteudo: %d\n",ini->conteudo);

    /* Testando função mostra(*ini) com dois elementos */
    printf("Função mostra: \n");
    mostra(ini);

    /* Remover segunda célula, com conteúdo 222 */
    printf("Sera removida a célula de valor: %d\n",((ini->prox)->prox)->conteudo);
    remover(ini->prox);
    mostra(ini);
    
    /*Inserir antes de 111 o valor 222 */
    buscaInsere(222,111,&ini);
    printf("Lista depois da inserção de 222 antes de 111\n");
    mostra(ini);

    /*Inserir antes de 444 o valor 555 */
    buscaInsere(555,444,&ini);
    printf("Lista depois da inserção de 555 antes de 444\n");
    mostra(ini);

    /*Inserir valor 789 no final, dado que não existe 123 */
    buscaInsere(789,123,&ini);
    printf("Lista depois da inserção de 789 no final \n");
    mostra(ini);

    /* Busca 444 */
    buscado = busca(444,ini);
    printf("Realmente eciste a célula com %d \n",buscado->conteudo);

    /*Remover valor 555 */
    buscaRemove(555,&ini);
    printf("Lista depois de removermos 555.\n");
    mostra(ini);

    /*Remover valor 333 */
    buscaRemove(333,&ini);
    printf("Lista depois de removermos 333.\n");
    mostra(ini);

    /*Remover valor 789 */
    buscaRemove(789,&ini);
    printf("Lista depois de removermos 789.\n");
    mostra(ini);

    /*Remover valor 123 */
    buscaRemove(123,&ini);
    printf("Lista depois de removermos 123, que não exsite.\n");
    mostra(ini);

    /*inverter listas*/
    puts("Lista invertida: \n");
    inverter(&ini);
    mostra(ini);

    return 0;
}

void inserir(int conteudo, Celula **ini){
    Celula *nova = (Celula*) malloc(sizeof(Celula));
    nova->conteudo = conteudo;
    nova->prox = *ini;
    *ini = nova;
}

void mostra(Celula *ini){
    Celula *p;
    for(p = ini; p != NULL; p = p->prox) {
        printf("%d\n",p->conteudo);
    }
}

void remover(Celula *celula){
    Celula *eliminada = celula->prox;
    celula->prox = celula->prox->prox;
    free(eliminada);
}

Celula *busca(int conteudo, Celula *ini){
    Celula *p;
    for(p=ini; p!=NULL; p=p->prox){
        if(p->conteudo == conteudo)
            return p;
    }
    return NULL;
}

void buscaInsere(int conteudo, int antes_conteudo, Celula **ini){
    /* Vamos varrear a lista com dois ponteiros p e q
     * sendo q sempre o próximo de p.
     * */
    Celula *p,*q;
    Celula *nova = (Celula*) malloc(sizeof(Celula));
    nova->conteudo = conteudo;

    /* Situação em que a lista está vazia ou o valor de antes_conteudo
     * é o primeiro item da lista, nos dois casos temos que mudar *ini */
    if(*ini == NULL || (*ini)->conteudo == antes_conteudo) {
        nova->prox = *ini;
        *ini = nova;
    } else {
        p = *ini;
        q = p->prox;
        while(q!=NULL && q->conteudo!=antes_conteudo) {
            /* p e q serão atualizados até encontrarmos o valor
             * procurado ou chegarmos até o final
             **/
            p = q;
            q = p->prox;
        }
        p->prox = nova;
        nova->prox = q;
    }
}

void buscaRemove(int conteudo, Celula **ini){
    Celula *p, *q;
    Celula *eliminada;

    /* Situação de lista vazia */
    if(*ini == NULL) {
        return;
    }
    /* Situação do valor buscado estar na primeira célula, teremos que mudar ini */
    if((*ini)->conteudo == conteudo){
        eliminada = *ini;
        *ini = eliminada->prox;
        free(eliminada);
        return;
    }
    /* Varrer a lista */
    p = *ini;
    q = p->prox;
    while(q!=NULL && q->conteudo!=conteudo) {
        p = q;
        q = p->prox;
    }
    /*Neste caso, se chegarmos no final, não fazemos nada*/
    if(q!=NULL) {
        eliminada = q;
        p->prox = q->prox;
        free(eliminada);
    }
}

void inverter(Celula **ini){
    Celula *p, *q, *r;
    p = NULL;
    q = *ini;
    while( q!=NULL) {
        r = q->prox;
        q->prox = p;
        p = q;
        q = r;
    }
    *ini = p;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

char *infixa2posfixa(char *infixa);

int main(int argc, char **argv) {
    char infixa[500] = "(A+B*C)";
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "(A+(B*C))");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A*(B+C)/D-E");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A+B*(C-D*(E-F)-G*H)-I*3");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A+B*C/D*E-F");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A+(B-(C+(D-(E+F))))");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    strcpy(infixa, "A*(B+(C*(D+(E*(F+G)))))");
    printf("Conversão de %s resultou em %s \n", infixa, infixa2posfixa(infixa));

    return 0;
}

char *infixa2posfixa(char *infixa){
    /* Inicializando pilha */
    int n;
    n = strlen(infixa);
    Stack s;
    s = stackInit(n);

    /* Indices para varrermos strings */
    int i_infixa = 0;
    int j_posfixa = 0;

    /* Inicializa posfixa */
    char *posfixa;
    posfixa = (char*) malloc( (n+1) * sizeof(char) );

    char item;

    while(i_infixa < n){
        switch(infixa[i_infixa]){
            case '(':
                stackPush(s,infixa[i_infixa]);
            break;

            case ')':
                while(!stackEmpty(s) && (item = stackPop(s)) !='('){
                    posfixa[j_posfixa++] = item;
                }
            break;

            case '+':
            case '-':
                while(!stackEmpty(s) && ((item = stackTop(s)) !='(') ){
                    posfixa[j_posfixa++] = stackPop(s);
                }
                stackPush(s,infixa[i_infixa]);
            break;

            case '*':
            case '/':
                while(!stackEmpty(s) && ((item = stackTop(s)) =='*' || item == '/')){
                    posfixa[j_posfixa++] = stackPop(s);
                }
                stackPush(s,infixa[i_infixa]);
            break;
            default:
                posfixa[j_posfixa++] = infixa[i_infixa];
        }
        i_infixa++;
    }

    printf("Olha quem sobrou na pilha: ");
    stackDump(s);

    /* Manda o que sobrou da pilha para posfixa */
    while(!stackEmpty(s)){
        posfixa[j_posfixa++] = stackPop(s);
    }

    posfixa[j_posfixa++] = '\0';
    stackFree(s);
    return posfixa;
}

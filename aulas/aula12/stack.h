typedef char Item;

struct stackNode {
    Item conteudo;
    struct stackNode *prox;
};
typedef struct stackNode *Link;

struct stack{
    Link topo;
};
/*O tipo stack é um ponteiro para um ponteiro? */
typedef struct stack *Stack;

Stack stackInit(int);
int stackEmpty(Stack);
void stackPush(Stack, Item);
Item stackPop(Stack);
Item stackTop(Stack);
void stackFree(Stack);
void stackDump(Stack);

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

Stack stackInit(int n){
    /* Vamos ignorar o n nessa implementação */
    /* Stack já é um ponteiro */
    Stack s = malloc(sizeof *s);
    s->topo = NULL;
    return s;
}

int stackEmpty(Stack s){
    return s->topo == NULL;
}
void stackPush(Stack s, Item item) {
    Link nova;
    nova = malloc(sizeof *nova);

    nova->conteudo = item;
    nova->prox = s->topo;
    s->topo = nova;
}
Item stackPop(Stack s) {
    Link eliminada;
    Item item;

    eliminada = s->topo;
    item = eliminada->conteudo;
    //printf("to aqui "); exit(0);
    s->topo = eliminada->prox;

    free(eliminada);

    return item;
}
Item stackTop(Stack s){
    return (s->topo)->conteudo;
}
void stackFree(Stack s){
    Link p;
    p = s->topo;
    while(p !=NULL){
        Link eliminada;
        eliminada = p->prox;
        free(eliminada);
        p = p->prox;
    }
}
void stackDump(Stack s){
    Link p;
    if(stackEmpty(s)) printf("Pilha atualmente vazia.");

    p = s->topo;
    while(p != NULL){
        printf("%c ", p->conteudo);
        p = p->prox;
    }
    fprintf(stdout, "\n");
}

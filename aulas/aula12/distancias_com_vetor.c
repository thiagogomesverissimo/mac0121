#include <stdio.h>
#include <stdlib.h>

int* distancias(int rede[][6], int n, int c);

int main(int argc, char **argv) {

    /* matriz da rede de conexões */
    int i, n = 6;
    int rede[6][6] = {
        {0,0,1,1,1,0},
        {0,0,1,0,1,0},
        {0,0,0,0,1,0},
        {0,0,0,0,1,1},
        {0,0,0,0,0,1},
        {0,1,0,0,0,0}
    };
    int *d;
    d = distancias(rede, n, 1);
    printf("Vetor das distâncias de 1: ");
    for(i = 0;i<n;i++){
        printf("%d ",d[i]);
    }
    
}

int* distancias(int rede[][6], int n, int c){

    /* vetor das distâncias */
    int *d;
    d = (int*) malloc(n*sizeof(int));

    /* fila com vetor */
    int *fila;
    fila = (int*) malloc(100*sizeof(int));
    int inicio =0;
    int fim = 0;

    int i,j,di;
    for(int i = 0; i < n; i++){
        d[i] = n;
    }
    d[c] = 0;

    /* enqueue(c) */
    fila[fim++] = c;

    /* !emptyQueue() */
    while(inicio != fim) {
        /* dequeue() */
        i = fila[inicio++];
        di = d[i];
        
        for(int j = 0; j < n; j++){
            if(rede[i][j] == 1 && d[j] > di+1){
                d[j] = di+1;
                /*enqueue(j)*/
                fila[fim++] = j; 
            }
        }
    }    

    return d;
}



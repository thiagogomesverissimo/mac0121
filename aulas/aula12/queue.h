typedef int Item;

void queueInit(int);
int queueEmpty();
void enqueue(Item);
Item dequeue();
void queueFree();

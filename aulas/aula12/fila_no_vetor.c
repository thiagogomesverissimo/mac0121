#include <stdio.h>
#include <stdlib.h>

void enqueue(int item, int *fila, int *fim, int n);
int dequeue(int *fila, int *inicio, int fim);
int vazia(int inicio, int fim);
int cheia(int fim, int n);
void mostra(int *fila, int inicio, int fim);

int main(int argc, char **argv) {
    
    /* Inicializa fila */
    int *fila;
    int removido, inicio, fim, n;
    n = 5;
    inicio= 0;
    fim = 0;
    fila = (int*) malloc(n * sizeof(int));

    enqueue(111,fila, &fim, n);
    enqueue(222,fila, &fim, n);
    enqueue(333,fila, &fim, n);
    enqueue(444,fila, &fim, n);
    enqueue(555,fila, &fim, n);
    enqueue(666,fila, &fim, n);
    printf("Atual situação da fila: \n");
    mostra(fila, inicio, fim);

    /*dequeue*/
    removido = dequeue(fila, &inicio, fim);
    printf("Dequeue elemento %d da fila: \n", removido);
    printf("Atual situação da fila: \n");
    mostra(fila, inicio, fim);

    dequeue(fila, &inicio, fim);
    dequeue(fila, &inicio, fim);
    dequeue(fila, &inicio, fim);
    dequeue(fila, &inicio, fim);
    dequeue(fila, &inicio, fim);
    dequeue(fila, &inicio, fim);
    dequeue(fila, &inicio, fim);
    printf("Atual situação da fila: \n");
    mostra(fila, inicio, fim);

    return 0;
}

void enqueue(int item, int *fila, int *fim, int n){
    if(cheia(*fim,n)){
        printf("Lista Cheia\n");
        return;
    }
    fila[(*fim)++] = item;
}

int dequeue(int *fila, int *inicio,int fim){
    if(vazia(*inicio, fim)) {
        printf("Lista vazia\n");
        return -1;
    }
    return fila[(*inicio)++];
}

void mostra(int *fila, int inicio, int fim){
    if(vazia(inicio, fim)) {
        printf("Lista vazia\n");
        return;
    }
    int i = inicio;
    while( i < fim ){
        printf("%d\n",fila[i]);
        i++;
    }
}

int vazia(int inicio, int fim){
    return inicio == fim;
}

int cheia(int fim, int n){
    return fim == n;
}

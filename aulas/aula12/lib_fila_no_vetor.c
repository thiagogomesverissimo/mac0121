#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

static Item *fila;
static int inicio;
static int fim;

void queueInit(int n){
    fila = (int*) malloc(n*sizeof(int));
    inicio = 0;
    fim = 0;
}

int queueEmpty(){
    return inicio == fim;
}


void enqueue(Item item){
    fila[fim++] = item;
}

Item dequeue(){
    return fila[inicio++];
}

void queueFree(){
    free(fila);
}

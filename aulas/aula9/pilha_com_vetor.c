#include<stdio.h>
#include<stdlib.h>

#define TRUE 1
#define FALSE 0
#define MAX 5

int isFull(int proximo_livre);
void push(int conteudo, int *proximo_livre, int *pilha);
int pop(int *proximo_livre, int *pilha);
void mostra(int proximo_livre,int *pilha);
int topo(int proximo_livre, int *pilha);

int main(int argc, char **argv){

    /* Inicializando pilha */
    int proximo_livre = 0;
    int *pilha;
    pilha = (int*) malloc(MAX * sizeof(int));

    push(666, &proximo_livre, pilha);
    push(555, &proximo_livre, pilha);
    push(444, &proximo_livre, pilha);
    push(333, &proximo_livre, pilha);
    push(222, &proximo_livre, pilha);

    /*Pilha cheia 111 não será inserido*/
    push(111, &proximo_livre, pilha);

    printf("Estado atual da pilha\n");
    mostra(proximo_livre, pilha);

    printf("Desempilhando: %d\n", pop(&proximo_livre, pilha));
    printf("Estado atual da pilha\n");
    mostra(proximo_livre, pilha);

    printf("Desempilhando: %d\n", pop(&proximo_livre, pilha));
    printf("Estado atual da pilha\n");
    mostra(proximo_livre, pilha);

    printf("Quem está no topo é: %d\n", topo(proximo_livre, pilha));

    pop(&proximo_livre, pilha);
    pop(&proximo_livre, pilha);
    pop(&proximo_livre, pilha);
    pop(&proximo_livre, pilha);
    pop(&proximo_livre, pilha);
    pop(&proximo_livre, pilha);
    pop(&proximo_livre, pilha);

    free(pilha);
    return 0;
}

void push(int conteudo, int *proximo_livre, int *pilha){
    if(!isFull(*proximo_livre)){
        pilha[(*proximo_livre)++] = conteudo;
    }
}

void mostra(int proximo_livre, int *pilha){
    int i;
    for(i = proximo_livre-1; i >=0;i--){
        printf("%d\n",pilha[i]);
    }
}

int isFull(int proximo_livre){
    if(proximo_livre == MAX)
        return TRUE;
    return FALSE;
}

int pop(int *proximo_livre, int *pilha){
    if(*proximo_livre != 0)
        return pilha[--(*proximo_livre)];
    return -1;
}

int topo(int proximo_livre,int *pilha){
    return pilha[proximo_livre - 1];
}

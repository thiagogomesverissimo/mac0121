A ideia desse arquivo é anotar os exemplos das aulas
que estou implementando e depois tentar implementá-los
olhando a descrição que eu mesmo fiz. Assim, eu confiro
com a implementação existente. Os protótipos das funções
estão incompletos de propósito e são apenas indicações de
como poderiam ser implentados, pois os parâmetros e o tipo
de retorno vai depender da estratégia de implementação.

## aula 6

### malloc_struct.c
Criar uma estrutura com alguns campos e alocar espaço
na memória com malloc para essa estrutura. A ideia aqui é
manipular a estrutura alocada através do retorno do malloc
que é um ponteiro e devemos manipular os campos com ->.

### vetor_dinamico.c
Alocar um vetor dinamincamente com malloc. Passando o sizeof 
do tipo os elementos desse vetor multiplicado pela quantidade de 
elementos do mesmo tipo. Liberar memória com o free fazendo processo
inverso.

### matriz_dinamica.c
Alocar do mesmo modo que vetor dinâmico, mas lembrar que a matriz 
é um vetor de vetores. Para liberar a memória com free fazer processo
inverso.

### lista_com_vetor.c
Na lista, os e elementos são colocados na sequência e portanto 
conhecemos a posição de cada um, podendo inserir ou remover os 
elementos numa dada posição.
Todos métodos devem ser implementados com uma versão iterativa 
e outra recursiva. Para controlar a lista os métodos podem ou não
receber a *lista e o *tamanho (vai depender da implementação).

Métodos:
 - inserir(posicao, elemento) ou inserir(posicao, elemento, *lista, *tamanho)
 - remover(posicao) ou remover(posicao, *lista, *tamanho)
 - busca(elemento) ou busca(elemento, *lista, *tamanho)
 - mostra() ou mostra(*lista, *tamanho)

### lista_ligada_com_vetor.c
Diferente da lista anterior, que é sequencial na memória, na lista ligada,
cada elemento, que vamos chamar agora de celula, terá um apontamento
para a seguinte.
Criar uma estrutura de célula com dois campos: conteúdo e próximo.
Nesta primeira implementação o próximo é o índice do vetor da
próxima célula (numa implementação futura sem vetores 
o próximo será um ponteiro).
Inicializar um vetor de tamanho fixo e fazer cada campo apontar 
para o próximo. A estratégia será manter uma variável inicio que
guardará o índice do vetor do início da lista e outra variável livre,
que guardará o índice do vetor da próxima posição livre na lista.
A última célula da lista apontará para -1, o que indicará que ela
é a última célula.

 - listaInit(vetor, livre, inicio)
 - inserirInicio(vetor, livre, inicio)
 - inserir(conteudo,apos_posicao,livre,vetor) 
 - remover(apos_posicao, livre, vetor)
 - busca(conteudo, vetor) 
 - buscaInsere(conteudo, livre, vetor)
 - mostra(inicio,vetor)

Etapas para implementar método inserir:
 - Como o vetor foi inicializado, todas células apontam para alguma
   outra célula, como vamos usar a célula livre no momento, vamos definir
   que a nova célula livre é essa apontada pela livre atual, que será usada
 - Depois de ter verificado quem será a próxima célula livre, podemos 
   usar a célula livre atual e colocar o valor do novo conteúdo.
 - Essa nova célula será inserida imediatamente após a célula apos_posicao
   passada como parâmetro. Assim, a próxima apontada pela nova célula 
   assumirá a próxima apontada por apos_posicao.
   Já apos_posicao passará a apontar para essa nova célula.

Dicas sobre o método remover: No método remover, apagamos a célula
apontada por apos_posicao, o que significa basicamente fazer apos_posicao
apontar para a célula apontada pela célula seguinte a apos_posicao. Essa
célula eliminada deve tornar-se a nova célula livre e a célula apontada por
pela essa nova livre, será a livre anterior.

## aula 7

### lista_ligada_sem_cabeca.c
Essa implementação é parecida com anterior, mas não usamos
vetor e sim alocamos cada célula na memória e os apontamentos
entre as células serão feitos com ponteiros, assim no struct
a variável proximo dever ser do tipo ponteiro. Como toda célula é
alocada dinamicamente não precisamos mais da variável livre.
Vamos manter uma célula chamada inicio que será do tipo ponteiro 
para célula e aponta para o início da lista, sendo NULL quando a 
lista está vazia. A última célula da lista apontará para NULL. 
Inserção será sempre no início.

Métodos:

 - inserir(conteudo, inicio)
 - mostra(inicio)
 - remove(proxima_da_celula)
 - busca(conteudo, inicio)
 - buscaInsere(conteudo,antes_conteudo,inicio)
 - buscaRemove(conteudo,inicio)

No método busca retornar um ponteiro ou NULL caso não encontre.
No método buscaInsere quando antes_conteudo não for encontrado,
inserir conteudo no final da lista. E deve ser tratada a situação
de quando a lista está vazia ou de quando o valor de antes_conteudo
é o primeiro da lista, pois nesses dois casos vamos ter que alterar 
o apontador inicio, o mesmo ocorre em buscaRemove. Nesses dois casos
de busca e inserção/remoção uma boa estratégia é varrer a lista
com dois ponteiros, um na sequência do outro, p e q.
Para inverter a lista usamos a estratégia de usar 3 ponteiros na 
sequência: p, q, r. Sendo que p começa em NULL, pois a primeira célula,
agora será a última, que aponta para NULL. q começa em ini.
Quando começamos a varredura, que vai até q chegar no final da lista,
fazemos 4 passos:

 1. r receberá o endereço de memória da próxima célula de q. 
 2. a célula q apontará para p
 3. avançamos p para posição de q
 4. avançamos q para posição de r

No final, a célula p será a nova inicio, pois ela está justamente
na última célula.

## aula 8

### lista_ligada_com_cabeca.c
Essa implementação é bem próxima da anterior, só que a lista
deve ser inicializada com uma célula que não será usada chamada
cabeça. Início apontará sempre para cabeça. O método para inverter
é o mesmo que o anterior, mas como agora a lista tem cabeça,
passamos como paramêtro para inversão inicio->prox, ou seja,
a cabeça.


Métodos:

 - inserir(conteudo, inicio)
 - mostra(inicio)
 - remove(proxima_de_celula)
 - busca(conteudo, inicio)
 - buscaInsere(conteudo,antes_conteudo,inicio)
 - buscaRemove(conteudo,inicio)
 - inverta(ini)

## aula 9

### pilha_com_vetor.c
Uma pilha é uma lista em que todas as operações são feitas em uma
extremidade, chamada de topo.
LIFO = Last In, First Out
stack overflow: tentar empilhar em um pilha cheia
stack underflow: tentar desempilhar em uma lista vazia

Métodos:

 - push(conteudo, pilha, livre)
 - pop(pilha,livre) remove o topo e retorna-o
 - isFull(proximo_livre)
 - mostra(proximo_livre, pilha)
 - topo(livre, pilha) - mostra quem está no topo sem remover

## aula 10

### infixa2posfixa_com_vetor.c
Implementar um método que converte uma string com notação
infixa para posfixa, usando a "ideia" de pilha implementada
sobre um vetor (não usando a implementação de pilha ainda).

Consersões que devem ser feitas:

 - Conversão de A+B*C resultou em ABC*+
 - Conversão de (A+(B*C)) resultou em ABC*+
 - Conversão de A*(B+C)/D-E resultou em ABC+*D/E-
 - Conversão de A+B*(C-D*(E-F)-G*H)-I*3 resultou em ABCDEF-*-GH*-*+I3*-
 - Conversão de A+B*C/D*E-F resultou em ABC*D/E*+F-
 - Conversão de A+(B-(C+(D-(E+F)))) resultou em ABCDEF+-+-+
 - Conversão de A*(B+(C*(D+(E*(F+G))))) resultou em ABCDEFG+*+*+*

## aula 11

### stack.h infixa2posfixa.c pilha_com_vetor.c
Implementar um método que converte uma string com notação
infixa para posfixa, mas que usara uma biblioteca de pilha
implementada sobre um vetor com os seguintes métodos:

 - void stackInit(int);
 - int stackEmpty();
 - void stackPush(Item);
 - Item stackPop();
 - Item stackTop();
 - void stackFree();
 - void stackDump();

Sendo o item definido como: typedef char Item;

Consersões que devem ser feitas:

 - Conversão de A+B*C resultou em ABC*+
 - Conversão de (A+(B*C)) resultou em ABC*+
 - Conversão de A*(B+C)/D-E resultou em ABC+*D/E-
 - Conversão de A+B*(C-D*(E-F)-G*H)-I*3 resultou em ABCDEF-*-GH*-*+I3*-
 - Conversão de A+B*C/D*E-F resultou em ABC*D/E*+F-
 - Conversão de A+(B-(C+(D-(E+F)))) resultou em ABCDEF+-+-+
 - Conversão de A*(B+(C*(D+(E*(F+G))))) resultou em ABCDEFG+*+*+*

### stack.h infixa2posfixa.c pilha_com_lista_ligada_com_cabeca.c
O ideia aqui é usar a mesma stack.h e a mesma infixa2posfixa.c.
Só vamos usar outra implementação de pilha. Ao invés de célula
vamos usar stackNode como estrutura:

    struct stackNode {
        Item conteudo;
        struct stackNode *prox;
    };
    typedef struct stackNode *Link;

## Aula 12

### stack.h infixa2posfixa.c pilha_com_lista_ligada_sem_cabeca.c
Agora vamos mudar stack.h e infixa2posfixa.c, pois vamos construir
métodos da pilha que receberão como parâmetro a Pilha, assim podemos
ter mais que uma pilha no mesmo programa. Implementar a fila sem cabeça.
Criar uma estrutura Stack desse modo:

    struct stackNode {
        Item conteudo;
        struct stackNode *prox;
    };
    typedef struct stackNode *Link;

    struct stack{
        Link topo;
    };
    typedef struct stack *Stack;

Métodos:

 - Stack stackInit(int);
 - int stackEmpty(Stack);
 - void stackPush(Stack,Item);
 - Item stackPop(Stack);
 - Item stackTop(Stack);
 - void stackFree(Stack);
 - void stackDump(Stack);

Consersões que devem ser feitas:

 - Conversão de A+B*C resultou em ABC*+
 - Conversão de (A+(B*C)) resultou em ABC*+
 - Conversão de A*(B+C)/D-E resultou em ABC+*D/E-
 - Conversão de A+B*(C-D*(E-F)-G*H)-I*3 resultou em ABCDEF-*-GH*-*+I3*-
 - Conversão de A+B*C/D*E-F resultou em ABC*D/E*+F-
 - Conversão de A+(B-(C+(D-(E+F)))) resultou em ABCDEF+-+-+
 - Conversão de A*(B+(C*(D+(E*(F+G))))) resultou em ABCDEFG+*+*+*

### fila_no_vetor.c
Primeira implementação de fila usando vetor.
Fila segue o padrão FIFO: First In First Out

Métodos:

 - enqueue(vetor)
 - dequeue(vetor)

### cidades distancias_com_vetor.c
Dado N cidades, podemos representar em uma matriz NxN as possíveis 
conexões direcionais entre essas cidades. Sendo as linhas i e colunas j 
as cidades, dizemos que rede[i][j] é igual a 1 se existe uma estrada 
que nos leva de i até j, mas não necessariamente o contrário. 
E rede[i][j] é zero quando não há uma estrada que nos leva de i até j. 
Dizemos ainda que rede[k][k] = 0.
Vamos calcular a quantidade de saltos que devemos dar de uma dada cidade
i até todas outras cidades N. Podemos colocar essas distâncias em um vetor
de tamanho N. Por fim, quando não for possível chegar até uma certa cidade k,
por exemplo, definimos que a distância de i até k é igual a N, ou seja, 
distancias[k] = N.

Rede que pode ser usada nos testes:

    int rede[6][6] = {
        {0,0,1,1,1,0},
        {0,0,1,0,1,0},
        {0,0,0,0,1,0},
        {0,0,0,0,1,1},
        {0,0,0,0,0,1},
        {0,1,0,0,0,0}
    };

Saídas esperadas:

 - Vetor das distâncias de 0: 0 3 1 1 1 2
 - Vetor das distâncias de 1: 6 0 1 6 1 2


## Aula 16

### busca_sequencial_vetor_ordenado.c
Dado x e um vetor ordenado v, encontra o índice da posição
de x em v, sequencialmente.

### busca_binaria_vetor_ordenado_iterativa.c busca_binaria_vetor_ordenado_recursiva.c
Dado x e um vetor ordenado v, encontra o índice da posição
de x em v. Vamos dividindo o vetor ao meio e se o valor no 
meio for menor do que o valo procurado, eu vou buscar na 
segunda metade, em caso contrário busco na primeira metade.

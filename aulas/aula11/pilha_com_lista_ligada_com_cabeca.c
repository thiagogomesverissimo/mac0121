#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

struct stackNode {
    Item conteudo;
    struct stackNode* prox;
};
typedef struct stackNode* Link;

static Link topo;

void stackInit(int n){
    /* Vamos ignorar o n nessa implementação */
    topo = malloc(sizeof *topo);
    topo->prox = NULL;
}

int stackEmpty(){
    return topo->prox == NULL;
}
void stackPush(Item item) {
    Link nova;
    nova = malloc(sizeof *nova);
    //printf("to aqui %c\n", item); exit(0);
    nova->conteudo = item;
    nova->prox = topo->prox;
    topo->prox = nova;
}
Item stackPop() {
    Link eliminada;
    Item item;

    eliminada = topo->prox;
    item = eliminada->conteudo;

    topo->prox = eliminada->prox;

    free(eliminada);
    return item;
}
Item stackTop(){
    return (topo->prox)->conteudo;
}
void stackFree(){
    Link p;
    p = topo->prox;
    while(p !=NULL){
        Link eliminada;
        eliminada = p->prox;
        free(eliminada);
        p = p->prox;
    }
}
void stackDump(){
    Link p;
    if(stackEmpty()) printf("Pilha atualmente vazia.");

    p = topo->prox;
    while(p != NULL){
        printf("%c ", p->conteudo);
        p = p->prox;
    }
    fprintf(stdout, "\n");
}

#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

static Item *pilha;
static int topo;

void stackInit(int n){
    pilha = (Item*) malloc(n * sizeof(Item));
    topo = 0;
}

int stackEmpty(){
    /* Em c:
     * 0 - FALSE
     * 1 - TRUE (na real, qq coisa diferente de zero)
     */
    return topo == 0;
}
void stackPush(Item item) {
    pilha[topo++] = item;
}
Item stackPop() {
    return pilha[--topo];
}
Item stackTop(){
    return pilha[topo-1];
}
void stackFree(){
    free(pilha);
}
void stackDump(){
    int i;
    if(stackEmpty()) printf("Pilha atualmente vazia.");
    for(i = topo-1; i >=0; i--){
        printf("%c ", pilha[i]);
    }
    fprintf(stdout, "\n");
}

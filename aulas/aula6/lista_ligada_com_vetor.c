#include<stdio.h>
#include<stdlib.h>

#define NULO -1;

/* Uma lista ligada é uma sequência de células, 
 * cada célula contém um objeto e o endereço da célula seguinte. */
struct celula {
    int conteudo;
    /** Neste caso o "endereço" da próxima célula é o index do vetor
     *  e não um endereço memória
     **/
    int prox;
};
typedef struct celula Celula;

void mostra(int inicio, Celula *v);
void insert(int conteudo, int p, int *livre, Celula *v);
void remover(int p, int *livre, Celula *v);


int main(){

    /*Lista*/
    Celula v[256];

    /* Vamos manter duas variáveis: uma apontando para o id do início da lista
     * e outra apontando para a próximo id livre */
    int inicio = 0;
    int livre = 0;

    int i;
    /* Inicializando lista, deixando todas apondando para próxima */
    for (i = 0; i < 256; i++) {
        v[i].prox = i+1;
    }

    /* Inicialização básica */
    v[0].conteudo = 8989;
    v[1].conteudo = 78798;
    v[2].conteudo = 852;
    v[3].conteudo = 398;
    v[3].prox = NULO;
    livre = 4;
    mostra(inicio,v);

    printf("\nInserindo 800 na posição apontada por 2\n");
    insert(800,2,&livre,v);
    mostra(inicio,v);

    printf("\nInserindo 750 na posição apontada por 1\n");
    insert(750,1,&livre,v);
    mostra(inicio,v);

    printf("\nRemovendo célula apontada por 0\n");
    remover(0,&livre,v);
    mostra(inicio,v);

    return 0;
}

void mostra(int inicio, Celula *v){
    int p;
    for(p = inicio; p != -1; p = v[p].prox){
        printf("Posição no vetor: %d; Conteúdo: %d; Próxima: %d \n",p,v[p].conteudo,v[p].prox);
    }
}

void insert(int conteudo, int p, int *livre, Celula *v) {
    int nova = *livre;

    /*A nova livre é a próxima apontada por livre*/
    *livre = v[*livre].prox;

    v[nova].conteudo = conteudo;

    /* Inserir depois da célula p*/
    v[nova].prox = v[p].prox;
    v[p].prox = nova;
}

/* Dado q, remove a célula depois de p */
void remover(int p, int *livre, Celula *v) {

    int eliminada = v[p].prox;
    v[p].prox = v[eliminada].prox;

    /*Transforma eliminada em livre e a próxima apondada será 
     a livre anterior */
    v[eliminada].prox = *livre;
    *livre = eliminada;
}

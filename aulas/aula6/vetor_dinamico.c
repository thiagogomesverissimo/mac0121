#include<stdio.h>
#include<stdlib.h>

void main(){
    int *vetor;
    int n,i;
    printf("Escolha um tamanho de vetor: ");
    scanf("%d",&n);

    vetor = malloc(n*sizeof(int));

    for(i = 0; i < n; i++) {
        *(vetor+i) = i;  /* mesmo que: v[i] = i */
    }

    /* Neste print é possivel ver que o vetor nada mais é que um 
     * apontamento para a primeira posição de memória 
     */
    printf("Posição da memória do vetor: %p\n", (void*)vetor);

    for(i = 0; i < n; i++) {
        printf("vetor[%d] = %d. Posição da memória: vetor[%d] = %p \n",i,vetor[i], i, (void*)(vetor+i) );
    }
    
    free(vetor);
}

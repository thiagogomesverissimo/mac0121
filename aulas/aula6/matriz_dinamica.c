#include<stdio.h>
#include<stdlib.h>

void main(){
    /* dimensão da matriz */
    int m,n;

    /*Índices para loops*/
    int i,j;

    /* Ponteiro para matriz */
    int **matriz;

    printf("Digite as dimensões (linha coluna) da matriz (exemplo: 4 5): ");
    scanf("%d %d",&m,&n);

    printf("Dimensões: %d linhas e %d colunas: ", m,n);

    /* se pensarmos na matriz como um vetor de vetores,
     * primeiros podemos malocar o vetor das linhas e 
     * cada linha tera que suportar um endereço de inteiro, 
     * por isso o sizeof(int*)*/
    matriz = malloc(m*sizeof(int*));

    /* agora malocamos as colunas */
    for(i = 0; i < m; i++){
        matriz[i] = malloc(n*sizeof(int));
    }

    /* vamos preencher as posições da matriz */
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            matriz[i][j] = i+j;
        }
    }

    /* Printar os valores da matriz */
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            printf("matriz[%d][%d] = %d \n",i,j,matriz[i][j]);
            printf("Posição de memória de matriz[%d][%d] = %p \n",i,j, (void*)( *(matriz+i)+j));
        }
    }

    /* Desmalocar tudo */
    for(int i = 0; i < m; i++) {
        free(matriz[i]);
    }
    free(matriz);
}

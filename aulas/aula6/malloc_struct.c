#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int dia, mes, ano;
} Data;

void main(){
    /* malloc retorna um endereço (ponteiro), assim vamos 
     * criar um ponteiro para guardar o retorno do malloc */
    Data *Aniversario;
    Aniversario = malloc(sizeof(Data));
    Aniversario->dia = 10;
    Aniversario->mes = 11;
    Aniversario->ano = 1986;

    printf("Sua data de nascimento é: %d/%d/%d\n",Aniversario->dia,Aniversario->mes,Aniversario->ano);
    free(Aniversario);
}

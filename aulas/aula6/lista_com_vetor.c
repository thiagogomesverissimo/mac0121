#include<stdio.h>
#include<stdlib.h>

/* métodos que vamos implementar */
int buscaI(int elemento, int tamanho, int *lista);
int buscaR(int elemento, int tamanho, int *lista);
int removeI(int posicao, int tamanho, int *lista);
int removeR(int posicao, int tamanho, int *lista);
/** As funções de inserção retornamo o novo tamanho
 *  ao que parece, a posição é dada começando por zero
 **/
int insereI(int posicao, int elemento, int tamanho, int *lista);
int insereR(int posicao, int elemento, int tamanho, int *lista);

void mostra(int tamanho, int *lista);

int main(){
    int *lista;
    int max = 256;
    int n = 0;

    lista = malloc(max*sizeof(int));

    n = insereI(0,78,n,lista);
    n = insereI(0,55,n,lista);
    n = insereI(1,58,n,lista);
    n = insereI(0,790,n,lista);
    n = insereR(0,96,n,lista);
    n = insereR(3,9901,n,lista);
    mostra(n,lista);

    printf("O elemento 55 está na posição %d\n",buscaI(55,n,lista));
    printf("O elemento 58 está na posição %d\n",buscaR(58,n,lista));
    printf("O elemento 550 está na posição %d (não existe)\n",buscaI(550,n,lista));
    printf("O elemento 580 está na posição %d (não existe)\n",buscaR(580,n,lista));

    printf("\nRemovendo o elemento 55 que está na posição 2");
    n = removeI(2,n,lista);
    mostra(n,lista);

    printf("\nRemovendo o elemento 58 que está na posição 3");
    n = removeR(3,n,lista);
    mostra(n,lista);

    free(lista);
    return 1;
}

void mostra(int tamanho, int *lista){
    int i;
    printf("\nTamanho da lista: %d\n", tamanho);
    for(i = 0; i < tamanho; i++) {
        printf("lista[%d] = %d\n",i,lista[i]);
    }
}

int insereI(int posicao,int elemento, int tamanho, int *lista){
    int i;

    /*Deslocar os elementos das posições a frente */
    for (i = tamanho; i > posicao; i--) {
        lista[i] = lista[i-1];
    }
    lista[posicao] = elemento;
    return tamanho+1;
}

int insereR(int posicao, int elemento, int tamanho, int *lista){
    if( posicao == tamanho) {
        lista[posicao] = elemento;
    } else {
        /* recursivamente podemos pensar que vamos inserir no final o 
        * elemento da posição anterior*/
        lista[tamanho] = lista[tamanho-1];
        insereR(posicao, elemento, tamanho-1, lista);
    }
    return tamanho + 1;
}

int buscaI(int elemento, int tamanho, int *lista){
    int i;
    for(i = 0; i < tamanho; i++){
        if(lista[i]==elemento) return i;
    }
    return -1;
}

int buscaR(int elemento, int tamanho, int *lista){
    if (tamanho == 0) return -1;

    if(lista[tamanho-1] == elemento) return tamanho -1;
    return buscaR(elemento, tamanho-1,lista);
}

int removeI(int posicao, int tamanho, int *lista){
    int i;
    for(i = posicao; i < tamanho; i++){
        lista[i] = lista[i+1];
    }
    return tamanho - 1;
}

int removeR(int posicao, int tamanho, int *lista) {
    lista[posicao] = lista[posicao+1];
    if( posicao != tamanho ) {
        removeR(posicao+1,tamanho,lista);
    }
    return tamanho-1;
}

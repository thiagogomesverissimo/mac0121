#include<stdio.h>
#include<stdlib.h>

struct celula {
    int conteudo;
    struct celula *prox;
};
typedef struct celula Celula;

/* Protótipos */
void inserir(int conteudo, Celula *ini);
void mostra(Celula *ini);
void remover(Celula *proxima_da_celula);
Celula* busca(int conteudo, Celula *ini);
void buscaInsere(int conteudo, int antes_do_conteudo, Celula *ini);
void buscaRemove(int conteudo, Celula *ini);
void inverter(Celula **ini);

int main(int argc, char **argv) {

    /* Inicialização da lista com cabeça */
    Celula *ini, *cabeca;
    cabeca = (Celula*) malloc(sizeof(Celula));
    cabeca->prox = NULL;
    ini = cabeca;

    /* Inserindo 111 na lista */
    inserir(111, ini);
    printf("Primeiro item da lista: %d\n",(ini->prox)->conteudo);

    /* Inserindo 222 na lista */
    inserir(222, ini);
    printf("Primeiro item da lista: %d\n",(ini->prox)->conteudo);

    /* Inserindo 333 na lista */
    inserir(333, ini);
    printf("Primeiro item da lista: %d\n",(ini->prox)->conteudo);

    /* Inserindo 444 na lista */
    inserir(444, ini);
    printf("Primeiro item da lista: %d\n",(ini->prox)->conteudo);

    /* Mostrando lista*/
    printf("Mostrando lista: \n");
    mostra(ini);

    /* Remover 333, que está depois de 444 */
    remover( ini->prox );
    printf("Lista depois que 333 foi removido \n");
    mostra(ini);

    /* Buscando 444 */
    printf("O valor %d foi encontrado com sucesso\n", (busca(444, ini)->conteudo ));

    /* Busca 222 e insere 333 antes dele */
    printf("Busca 111 e insere 222 antes dele: \n");
    buscaInsere(333,222,ini);
    mostra(ini);

    /* Busca 123 que não existe e insere 789 no final */
    printf("Busca 123 que não existe e insere 789 no final: \n");
    buscaInsere(789,123,ini);
    mostra(ini);

    /* Remove 222 */
    printf("Remove 222 \n");
    buscaRemove(222,ini);
    mostra(ini);

    printf("Remove 789 \n");
    buscaRemove(789,ini);
    mostra(ini);

    /* Remove 444 */
    printf("Remove 444 \n");
    buscaRemove(444,ini);
    mostra(ini);

    /* Remove 567 */
    printf("Remove 567, que não existe \n");
    buscaRemove(567,ini);
    mostra(ini);

    /* Inserindo 444,555,666 e 777 na lista */
    inserir(444, ini);
    inserir(555, ini);
    inserir(666, ini);
    inserir(777, ini);
    printf("Lista com 444, 555, 666 e 777:\n");
    mostra(ini);

    /* Invertendo lista */
    printf("Lista invertida :\n");
    inverter(&ini->prox);
    mostra(ini);

    return 0;
}

void inserir(int conteudo, Celula *ini) {
    /* Lembrar que inserções são feitas no início */
    Celula *nova;
    nova = (Celula*) malloc(sizeof(Celula));
    nova->conteudo = conteudo;
    nova->prox = ini->prox;
    ini->prox = nova;
}

void mostra(Celula *ini){
    Celula *p;
    p = ini->prox;
    while(p!=NULL){
        printf("%d\n",p->conteudo);
        p = p->prox;
    }
}

void remover(Celula *proxima_da_celula) {
    Celula *eliminada = proxima_da_celula->prox;
    proxima_da_celula->prox = (proxima_da_celula->prox)->prox;
    free(eliminada);
}

Celula* busca(int conteudo, Celula* ini){
    Celula *p;
    p = ini->prox;
    while(p!=NULL){
        if(p->conteudo == conteudo)
            return p;
    }
    return NULL;
}

void buscaInsere(int conteudo, int antes_do_conteudo,Celula *ini){
    Celula *p, *q, *nova;

    /* Nova celula*/
    nova = (Celula*) malloc(sizeof(Celula));
    nova->conteudo = conteudo;

    p = ini;
    q = p->prox;
    while(q!=NULL && q->conteudo!=antes_do_conteudo) {
        p = q;
        q = p->prox;
    }
    p->prox = nova;
    nova->prox = q;
}

void buscaRemove(int conteudo, Celula *ini){
    Celula *p, *q;
    p = ini;
    q = p->prox;
    while(q!=NULL && q->conteudo!=conteudo){
        p = q;
        q = p->prox;
    }
    if(q != NULL){
        p->prox = q->prox;
        free(q);
    }
}

void inverter(Celula **ini){
    Celula *p, *q, *r;
    p = NULL;
    q = *ini;

    while(q != NULL){
        r = q->prox;
        q->prox = p;
        p = q;
        q = r;
    }
    *ini = p;
}

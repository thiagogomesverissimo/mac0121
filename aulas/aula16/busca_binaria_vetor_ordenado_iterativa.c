#include<stdio.h>
#include<stdlib.h>

int busca(int x, int *v, int n);

int main(int argc, char **argv){
    int p,x,n;
    int v[] = {10,20,25,35,38,40,44,50,55,65,99};

    /* Tamanho do vetor */
    n = 11;
    /* Elemento buscado */
    x =  atoi(argv[1]);
    /* Posição encontrada */
    p = busca(x,v,n);
    if(p == -1) printf("%d não está no vetor\n",x);
    else printf("%d está na posição %d\n",x,p);

    exit(0);
}


int busca(int x, int *v, int n){
    int inicio = 0;
    int fim = n-1;
    int meio;

    while( inicio <= fim  ){
        meio = (inicio+fim)/2;
        if(v[meio] == x) return meio;
        /* Se o meio for menor do que o valo procurado, 
         * eu vou buscar na segunda metade, em caso contrário busco 
         * na primeira  parte*/
        if(v[meio] < x) inicio = meio+1;
        else fim = meio-1;
    }
    return -1;
}

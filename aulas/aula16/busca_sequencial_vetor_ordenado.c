#include<stdio.h>
#include<stdlib.h>

int busca(int x, int *v, int n);

int main(int argc, char **argv){
    int p,x,n;
    int v[] = {10,20,25,35,38,40,44,50,55,65,99};

    /* Tamanho do vetor */
    n = 11;
    /* Elemento buscado */
    x =  atoi(argv[1]);
    /* Posição encontrada */
    p = busca(x,v,n);
    if(p == -1) printf("%d não está no vetor\n",x);
    else printf("%d está na posição %d\n",x,p);

    exit(0);
}


int busca(int x, int *v, int n){
    int i = 0;
    /* Como o vetor está ordenado, eu avanço até  */
    while( i < n && v[i] < x ) i++;
    if(v[i]==x) return i;
    return -1;
}

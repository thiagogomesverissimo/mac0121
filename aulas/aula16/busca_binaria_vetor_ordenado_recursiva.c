#include<stdio.h>
#include<stdlib.h>

int busca(int x, int *v, int inicio, int fim);

int main(int argc, char **argv){
    int p,x,n;
    int v[] = {10,20,25,35,38,40,44,50,55,65,99};

    /* Tamanho do vetor */
    n = 11;
    /* Elemento buscado */
    x =  atoi(argv[1]);
    /* Posição encontrada */
    p = busca(x,v,0,n-1);
    if(p == -1) printf("%d não está no vetor\n",x);
    else printf("%d está na posição %d\n",x,p);

    exit(0);
}


int busca(int x, int *v, int inicio, int fim){
    int meio;
    meio = (inicio+fim)/2;
    if(inicio > fim) return -1;

    if(v[meio] == x) return meio;

    if(v[meio] < x) return busca(x,v,meio+1,fim);
    else return busca(x,v,inicio, meio-1);
}

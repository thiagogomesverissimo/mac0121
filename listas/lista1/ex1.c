/* Escreva uma função com protótipo
 * int soma (Celula *ini);
 * que recebe uma lista encadeada ini de números inteiros e devolve 
 * a soma dos números na lista. Suponha que a lista encadeada 
 * não tem cabeça de lista.
 */
#include<stdio.h>
#include<stdlib.h>

struct celula{
    int conteudo;
    struct celula *prox; 
};

typedef struct celula Celula;

int soma(Celula *ini);

int main(int argc, char **argv) {

    /* Inicializando uma lista encadeada sem cabeça de exemplo */
    Celula *celula4 = (Celula*) malloc(sizeof(Celula));
    Celula *celula3 = (Celula*) malloc(sizeof(Celula));
    Celula *celula2 = (Celula*) malloc(sizeof(Celula));
    Celula *celula1 = (Celula*) malloc(sizeof(Celula));
    Celula *ini;

    celula4->conteudo = 400;
    celula4->prox = NULL;

    celula3->conteudo = 300;
    celula3->prox = celula4;
    
    celula2->conteudo = 200;
    celula2->prox = celula3;

    celula1->conteudo = 100;
    celula1->prox = celula2;

    ini = celula1;

    printf("A soma dos itens da lista ligada é: %d \n",soma(ini));
    return 0;
}

int soma(Celula *ini){
    Celula *p;
    p = ini;
    int soma = 0;
    while(p!=NULL){
        soma += p->conteudo;
        p = p->prox;
    }
    return soma;
}
